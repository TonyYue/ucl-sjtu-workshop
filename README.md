# UCL-SJTU Workshop

This is the repository contains the necessary files and links used in this workshop. 

Date: 28th Feb 2022 to 2nd Mar 2022 (may vary depends on the situation)

## Prerequisites

### Rivet 

CERN lxplus directory will be used as the working area. Please create your own working directory there, or use your only insititue cluster if the rivet environment is available on it. 

Or if you do not have an CERN account, please head to https://docs.docker.com/install and install Docker

You can check if Docker is correctly installed by doing

```
   docker run hello-world

```

This will run the hello-world Docker image.

Please make sure do:

```
   wget "https://rivetval.web.cern.ch/rivetval/TUTORIAL/truth-analysis.tar.gz" -O- | tar -xz --no-same-owner
```

to get the HepMC files that we are going to use during the workshop.

### Contur

Docker will be used for Contur, you can download the latest image via: 

  ```
  docker pull hepstore/contur-herwig:main

  ```
In addition, some additional files used in this workshop for running Contur is provided here: https://cernbox.cern.ch/index.php/s/SJmYPBwY9HxVYQp
Please download this in advance the workshop. 

If you would like to get a local setup for Contur, please follow the instruction in the README of this page: https://gitlab.com/hepcedar/contur and install Herwig, Rivet and Contur packages.

